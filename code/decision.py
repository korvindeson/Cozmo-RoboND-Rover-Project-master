import numpy as np
import math
from cozmo.util import distance_mm, speed_mmps, degrees
# This is where you can build a decision tree for determining throttle, brake and steer 
# commands based on the output of the perception_step() function

def updcoords(distance,angle,Rover):
    X=distance*math.cos(angle) + Rover.pos[0]
    Y=distance*math.sin(angle) + Rover.pos[1]
    return X,Y


def decision_step(Rover,robot):

    # Implement conditionals to decide what to do given perception data
    # Here you're all set up with some basic functionality but you'll need to
    # improve on this decision tree to do a good job of navigating autonomously!

    #if rover near sample, collect it    
    if Rover.near_sample > 0 and Rover.picking_up == 0:
        Rover.send_pickup = True
        Rover.near_sample == False
    else:
        #Rover.send_pickup = False
        #Rover.near_sample = 0 
        #Rover.picking_up = 0
        #Rover.mode = 'forward'
        pass
    
    # Example:
    # Check if we have vision data to make decisions with
    if Rover.nav_angles is not None:
        # Check for Rover.mode status
        print(len(Rover.nav_angles))
        if Rover.mode == 'forward': 
            # Check the extent of navigable terrain
            
            if len(Rover.nav_angles) >= Rover.stop_forward:  
                # If mode is forward, navigable terrain looks good 
                # and velocity is below max, then throttle 
                if Rover.vel < Rover.max_vel:
                    # Set throttle value to throttle setting
                    Rover.throttle = Rover.throttle_set
                else: # Else coast
                    Rover.throttle = 0
                Rover.brake = 0
                # Set steering to average angle clipped to the range +/- 15
                Rover.steer = np.clip(np.average(Rover.nav_angles * 180/np.pi), -50, 50)
                #get cozmo to move
                robot.drive_straight(distance_mm(50), speed_mmps(Rover.vel*100),in_parallel=True).wait_for_completed()
                Rover.pos[0],Rover.pos[1] = updcoords(50,Rover.yaw,Rover)
                
                if abs(Rover.steer) > 35:
                    robot.turn_in_place(degrees(Rover.steer),in_parallel=True) #.wait_for_completed()
                    Rover.yaw += Rover.steer
            # If there's a lack of navigable terrain pixels then go to 'stop' mode
            elif len(Rover.nav_angles) < Rover.stop_forward:
                    # Set mode to "stop" and hit the brakes!
                    Rover.throttle = 0
                    # Set brake to stored brake value
                    Rover.brake = Rover.brake_set
                    Rover.steer = 60
                    Rover.mode = 'stop'
                    
                    robot.turn_in_place(degrees(Rover.steer),in_parallel=True).wait_for_completed()
                    Rover.yaw += Rover.steer
                    
        # If we're already in "stop" mode then make different decisions
        elif Rover.mode == 'stop':
            # If we're in stop mode but still moving keep braking
            #if Rover.vel > 10:
            #    Rover.throttle = 0
            #    Rover.brake = Rover.brake_set
            #    Rover.steer = 0
            # If we're not moving (vel < 0.2) then do something else
            #if Rover.vel <= 10:
            # Now we're stopped and we have vision data to see if there's a path forward
            if len(Rover.nav_angles) < Rover.go_forward:
                #throt = [0,Rover.throttle_set]
                Rover.throttle = Rover.throttle_set
                # Release the brake to allow turning
                Rover.brake = 0
                # Turn range is +/- 15 degrees, when stopped the next line will induce 4-wheel turning
                st=[-50,50]
                Rover.steer = st[np.random.randint(0,1)] # Could be more clever here about which way to turn
                
                robot.turn_in_place(degrees(Rover.steer),in_parallel=True) #.wait_for_completed()
                Rover.yaw += Rover.steer
                
            # If we're stopped but see sufficient navigable terrain in front then go!
            if len(Rover.nav_angles) >= Rover.go_forward:
                # Set throttle back to stored value
                Rover.throttle = Rover.throttle_set
                # Release the brake
                Rover.brake = 0
                # Set steer to mean angle
                Rover.steer = np.clip(np.mean(Rover.nav_angles * 180/np.pi), -50, 50)
                Rover.mode = 'forward'
                
                robot.drive_straight(distance_mm(50), speed_mmps(Rover.vel*100),in_parallel=True).wait_for_completed()
                Rover.pos[0],Rover.pos[1] = updcoords(50,Rover.yaw,Rover)
                #robot.turn_in_place(degrees(Rover.steer),in_parallel=True) #.wait_for_completed()
                
                
        #new mode. Turn with no gas or breaks
        elif Rover.mode == 'turn':
            Rover.brake=0
            Rover.throttle = 0
            st=[-50,50]
            Rover.steer = st[np.random.randint(0,1)]
            Rover.mode = 'stop'
            
            
            robot.turn_in_place(degrees(Rover.steer),in_parallel=True).wait_for_completed()
            Rover.yaw += Rover.steer
            
        elif Rover.mode == 'go_for_rock':
            pass
    # Just to make the rover do something 
    # even if no modifications have been made to the code
    else:
        Rover.throttle = Rover.throttle_set
        Rover.steer = 0
        Rover.brake = 0
    
        robot.drive_straight(distance_mm(50), speed_mmps(Rover.vel*100),in_parallel=True).wait_for_completed()
        Rover.pos[0],Rover.pos[1] = updcoords(50,Rover.yaw,Rover)
    #unstuck
    '''
    if Rover.brake == Rover.brake_set and Rover.vel < 10:
        Rover.brake=0
        Rover.throttle = 0
        st=[-50,50]
        Rover.steer = st[np.random.randint(0,1)]
        Rover.mode = 'turn'
    if Rover.vel < 0.2:
        st=[-50,50]
        Rover.steer = st[np.random.randint(0,1)]
        Rover.mode = 'turn'
    if Rover.steer != 0 and Rover.throttle == 0.2 :    
        Rover.mode = 'turn'
    '''
    return Rover

