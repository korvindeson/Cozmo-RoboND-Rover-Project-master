import cozmo
from PIL import Image
import time
import numpy as np
import cv2
from matplotlib import pyplot as plt
# Load an color image in grayscale
import sys
sys.path.append('../code/')
import perception, decision
from cozmo.util import distance_mm, speed_mmps, degrees
import matplotlib.image as mpimg


# Define RoverState() class to retain rover state parameters
class RoverState():
    def __init__(self):
        self.start_time = None # To record the start time of navigation
        self.total_time = None # To record total duration of naviagation
        self.img = None # Current camera image
        self.pos = [120,100] # Current position (x, y)
        self.def_pos = [120,100]
        self.yaw = 0 # Current yaw angle
        self.pitch = 0 # Current pitch angle
        self.roll = 0 # Current roll angle
        self.vel = 1 # Current velocity
        self.steer = 0 # Current steering angle
        self.throttle = 0 # Current throttle value
        self.brake = 0 # Current brake value
        self.nav_angles = 0 # Angles of navigable terrain pixels
        self.nav_dists = 0 # Distances of navigable terrain pixels
        #self.ground_truth = ground_truth_3d # Ground truth worldmap
        self.mode = 'forward' # Current mode (can be forward or stop)
        self.throttle_set = 10 # Throttle setting when accelerating
        self.brake_set = 2 # Brake setting when braking
        # The stop_forward and go_forward fields below represent total count
        # of navigable terrain pixels.  This is a very crude form of knowing
        # when you can keep going and when you should stop.  Feel free to
        # get creative in adding new fields or modifying these!
        self.stop_forward = 35000 # Threshold to initiate stopping
        self.go_forward = 40000 # Threshold to go forward again
        self.max_vel = 2 # Maximum velocity (meters/second)
        # Image output from perception step
        # Update this image to display your intermediate analysis steps
        # on screen in autonomous mode
        
        self.vision_image = np.zeros((160, 320, 3), dtype=np.float) 
        # Worldmap
        # Update this image with the positions of navigable terrain
        # obstacles and rock samples
        self.worldmap = np.zeros((240, 200, 3), dtype=np.float) 
        self.samples_pos = [] # To store the actual sample positions
        self.samples_found = [] # To count the number of samples found
        self.near_sample = 0 # Will be set to telemetry value data["near_sample"]
        self.picking_up = 0 # Will be set to telemetry value data["picking_up"]
        self.send_pickup = False # Set to True to trigger rock pickup
        self.samples_found_old = []
        self.nav_angles_old = []
        # This bool is used to track beginning end end of rock load process
        # otherwise we receave +200 rocks per one rock.
        self.onetime=True
        


#build dict from Cozmo telemetry
def builddata(Rover):
    data={}
    
    return data

#initialize cozmo, run app
def cozmo_program(robot : cozmo.robot.Robot):
    i=1
    Rover = RoverState()
    while 1:
        #print(x.image)
        robot.camera.image_stream_enabled = True    
        
        robot.set_head_angle(degrees(-30),in_parallel=True).wait_for_completed()
        robot.move_lift(20) #.wait_for_completed()
        # wait for a new camera image to ensure it is captured properly
        image = robot.world.latest_image
        while image is None:
               time.sleep(0.1)
               image = robot.world.latest_image
       
        
    
        # Initialize our rover 
        
        
        #save image to the disc
        cv2.imwrite("./img/image_rover.png", np.array(robot.world.latest_image.raw_image))
        
        #read image
        image = cv2.imread('./img/image_rover.png',0)
        
        #save edges to rover
        Rover.img = cv2.Canny(image,100,200)
        
        #show curent edges before perception
        
        
        # save logs
        mpimg.imsave("./img/image_rover_{}.png".format(i), np.array(Rover.img))
        
        # perception step
        Rover = perception.perception_step(Rover)
        # decision step
        Rover = decision.decision_step(Rover,robot)
        
        print(Rover.mode)
        print(Rover.pos)
        
        
        cv2.namedWindow('Cozmo View')
        cv2.imshow('Cozmo View',cv2.Canny(image,210,220))
        
        cv2.waitKey(1)
        
        cv2.namedWindow('Processed View')
        cv2.imshow('Processed View',Rover.vision_image)
        cv2.waitKey(1)
        
        
        
        mpimg.imsave("./img/image_processed_{}.png".format(i), np.array(Rover.vision_image))
        mpimg.imsave("./img/image_rover_native_{}.png".format(i), np.array(robot.world.latest_image.raw_image))
        
        
        i+=1


if __name__ == '__main__':
    cozmo.run_program(cozmo_program)

