# About

This project is based on Udacity self-driving rover project for robotics nanodegree (https://github.com/korvindeson/Rover-RoboticsProject). Self-driving system was implemented with Cozmo.
You can turn it with python 2.5+ and 3+.

# Includes

You will need python 2.5+, numpy, cv2, PIL, matplotlib

# Files

1) cozmo-python-sdk-master/run-cozmo.py - analog for drive rover from original project
2) code/decision.py - module where decisions are made
3) code/perception.py - module, where we process visual information
4) cozmo-python-sdk-master/img/* - pictures: rover view; edge detection process with generated map; final angle selection.

# Results

https://youtu.be/IlCRH4VzyJs
